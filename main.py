# currently this file is used as a test to make sure everything is working

from model_card import ModelCard
from typing import Dict, Any
from utils import Project, Creator, Owner, ApprovalProcess
import yaml
import argparse

def unpack(config: Dict[str, Any]) -> ModelCard:
    """Function to unpack a yaml config file

    Parameters:
    -----------
    config: {str : Any}
        A dictionary that is the raw output of yaml.load()

    Returns:
    --------
    ModelCardParams
        A fully instantiated ModelCardParams instance
    """
    try:
        project = Project(language_code=config['project']['language_code'],
                          project=config['project']['project'],
                          subdomain=config['project']['subdomain'])
    except:
        print("Project is not properly configured in config.yaml")
        
    try:
        creators = []
        for creator in config['creators']:
            creators.append(Creator(name=creator['name'],
                                    email=creator['email'],
                                    link=creator['link']))
    except:
        print("Creators are not properly configured in config.yaml")
        
    try:
        owner = Owner(name=config['owner']['name'],
                      email=config['owner']['email'],
                      link=config['owner']['link'])
    except:
        print("Owner is not properly configured in config.yaml")
        
    try:
        approval_process = ApprovalProcess(
            description=config['approval_process']['description'],
            forums=config['approval_process']['forums']
        )
    except:
        print("Approval processes are not properly configured in config.yaml")
        
    # TODO: add elifs for DataSheet and ServiceSheet
    if config['type'] == "model":

        param_dict = dict(
            title=config['title'],
            project=project,
            creator=creators,
            owner=owner,
            motivation=config['motivation'],
            intended_users=config['intended_users'],
            intended_uses=config['intended_uses'],
            banned_uses=config['banned_uses'],
            most_recent_deploy=config['most_recent_deploy'],
            approval_process=approval_process,
            deprecation_conditions=config['deprecation_conditions'],
            license=config['license'],
            mitigations=config['mitigations'],
            retraining=config['retraining'],
            dataset=config['dataset'],
            tier=config['tier']
        )

        if config['tier'] <= 2:
            param_dict['train_data_url'] = config['train_data_url']
            param_dict['model_info_url'] = config['model_info_url']
            
        if config['tier'] <= 1:
            param_dict['sensitive_features'] = config['sensitive_features']
            param_dict['label'] = config['label']
            param_dict['test_data_url'] = config['test_data_url']
            param_dict['model_url'] = config['model_url']
            param_dict['api'] = config['api']
        
        return ModelCard(**param_dict)
        

if __name__ == "__main__":
    
    parser = argparse.ArgumentParser(description='A script that creates model cards')
    parser.add_argument(
        '--config',
        type=str,
        nargs=1,
        help='The configuration file that contains all metadata/data about the model card'
    )

    args = parser.parse_args()

    config_path = args.config[0]

    with open(config_path) as f:
        config_params = yaml.load(f)

    model_card = unpack(config=config_params)

    if model_card.tier == 1:
        print("Tier 1 model card\n")
        
        print("loading model")
        model_card.load_self()

        print("loading model info")
        model_card.load_model_info()

        print("loading dataset")
        model_card.load_dataset()

        print("scoring dataset")
        model_card.score_model()

        print("conducting analysis on data")
        analyses = model_card.do_analysis()

        print("converting findings to wikitext")
        wikitext = model_card.create_model_card_wikitext(analyses)

        print("writing to meta.wikimedia.org")
        model_card.write_to_wiki(wikitext)
        print("done")
    elif model_card.tier == 2:
        print("Tier 2 model card\n")

        print("loading model info")
        model_card.load_model_info()
        print("converting model info to wikitext")
        wikitext = model_card.create_model_card_wikitext()

        print("writing to meta.wikimedia.org")
        model_card.write_to_wiki(wikitext)
        print("done")

    elif model_card.tier == 3:
        print("Tier 3 model card\n")

        print("converting model info to wikitext")
        wikitext = model_card.create_model_card_wikitext()

        print("writing to meta.wikimedia.org")
        model_card.write_to_wiki(wikitext)
        print("done")
