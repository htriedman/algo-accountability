import base64
import nltk
from editquality.feature_lists import enwiki
import json
import mwapi
import pandas as pd
import pickle
import requests
import io
import ast
from revscoring.extractors.api import extractor
from revscoring.scoring.models.model import Model
from typing import List, Dict, Any, Tuple

nltk.download('stopwords')

def get_model(url: str) -> Model:
    """Fetches revscoring model from an external url

    Parameters:
    -----------
    url : str
        A url where the revscoring model lives

    Returns:
    --------
    Model
        A compiled revscoring model that was fetched from the URL
    """
    resp = requests.get(url)

    return Model.load(io.BytesIO(resp.content), error_on_env_check=False)

def get_model_info(url: str) -> Tuple[str, str, str]:
    """Fetches and parses revscoring model info from an external url

    Parameters:
    -----------
    url : str
        A url where the revscoring model lives

    Returns:
    --------
    [str]
        List of parsed strings pertaining to the training info of a model
    """
    resp = requests.get(url)
    content = resp.content.decode("utf-8")

    # split into model_info + env and stats + schema
    (info_env, stats_schema) = content.split("\n\t\n\tStatistics:\n\t")

    # remove environment and get model info as list
    (model_info, _) = info_env.split("\n\tEnvironment:")
    model_info_list = model_info.split("\n\t - ")

    # convert list of model info strings to nicely formatted json dict
    model_info_dict = {}
    for i, entry in enumerate(model_info_list):
        if i == 0:
            continue
        (k, v) = entry.split(": ", maxsplit=1)
        if k == "params":
            v = v.split(", 'label_weights': ")[0] + "}"
            v = ast.literal_eval(v)
        model_info_dict[k] = v
        model_info = json.dumps(model_info_dict, indent=4)

    # split stats and score schema
    (stats, score_schema) = stats_schema.split("\n\t\n\t - score_schema: ")

    # format score schema into nice json string
    score_schema = json.dumps(ast.literal_eval(score_schema), indent=4)

    return model_info, score_schema, stats

def get_labels(url: str) -> Dict[int, Dict[str, Any]]:
    """Fetches label data directly from an external domain

    Parameters:
    -----------
    url : str
        A url where the data lives in json format

    Returns:
    --------
    dictionary : {rev_id : {attribute : value}}
        A dictionary that maps revisions to most basic labels
    """
    resp = requests.get(url)
    data = resp.json()
    revisions = {}

    # TODO: make extensible to other model schema
    for label in data['tasks']:

        # skip if unlabeled
        if len(label['labels']) == 0:
            continue

        # skip if unsure
        if label['labels'][0]['data']['unsure']:
            continue

        # skip if incomplete labels
        if label['labels'][0]['data']['damaging'] is None or label['labels'][0]['data']['goodfaith'] is None:
            continue

        rev_id = int(label['data']['rev_id'])
        revisions[rev_id] = {}
        revisions[rev_id]['auto_labeled'] = False
        revisions[rev_id]['goodfaith'] = label['labels'][0]['data']['goodfaith']
        revisions[rev_id]['damaging'] = label['labels'][0]['data']['damaging']
        revisions[rev_id]['autolabel'] = {}
    return revisions

def get_all_features(revisions: Dict[int, Dict[str, Any]], subdomain: str, label: str) -> Dict[int, Dict[str, Any]]:
    """Uses the revscoring extractor API to get all relevant data for a given dataset.

    Parameters:
    -----------
    revisions : {rev_id : {attribute : value}}
        Dictionary that maps revision ids to labels and features
    subdomain : str
        The subdomain to get revision data from
    label : str
        The label (either "goodfaith" or "damaging") to pull data for

    Returns:
    --------
    dictionary : {rev_id : {attribute : value}}
        Dictionary that maps revision ids to labels and features

    Raises:
    -------
    ValueError
        If label to pull data for is invalid
    """
    session = mwapi.Session(f"https://{subdomain}.org")
    e = extractor.Extractor(session)
  
    try:
        if label == "goodfaith":
            dependent = enwiki.goodfaith
        elif label == "damaging":
            dependent = enwiki.damaging
    except ValueError:
        return "Invalid label"

    for rev_id, rev in zip(revisions.keys(), e.extract(revisions.keys(), dependent)):
        try:
            for feature, value in zip(dependent, rev[1]):
                revisions[rev_id][feature.name] = value
        except:
            continue

    return revisions

def get_metadata(revisions: Dict[int, Dict[str, Any]], subdomain: str) -> Dict[int, Dict[str, Any]]:
    """Takes in the decoded set of revisions returned by `get_decoded_revisions` and a
    language and returns the same dictionary with relevant metadata, like page title,
    username, timestamp, comment, etc.

    Parameters:
    -----------
    revisions : {rev_id : {attribute : value}}
        A dictionary of revisions output by `get_decoded_revisions`
    subdomain : str
        A mediawiki project in the form of <language>.<project>, e.g. en.wikipedia

    Returns:
    --------
    dictionary : {rev_id : {attribute : value}}
        Dictionary that maps rev_ids to rows represented by attribute value pairs with
        extra metadata attached 
    """
    session = mwapi.Session(f'https://{subdomain}.org')

    for rev_id in revisions.keys():

        s = session.get(action='query', prop='revisions', revids=rev_id, rvprop='ids|timestamp|flags|comment|user|tags')
        try:
            for rev in s['query']['pages'].values():
                revisions[rev_id]['title'] = rev['title']
                revisions[rev_id]['user'] = rev['revisions'][0]['user']
                revisions[rev_id]['timestamp'] = rev['revisions'][0]['timestamp']
                revisions[rev_id]['comment'] = rev['revisions'][0]['comment']

                revisions[rev_id]['tags'] = rev['revisions'][0]['tags']
        except:
            continue

    return revisions

def get_sensitive_features(df: pd.DataFrame, features: List[Dict[str, Any]]) -> pd.DataFrame:
    """Gets a list of sensitive features and filters the df to just those features.

    Parameters:
    -----------
    df : pd.DataFrame
        Dataframe from which the data will be pulled
    features: [str]
        List of sensitive features
    """
    feature_names = [f['col'] for f in features]
    return df[feature_names]

def get_data(url: str, subdomain: str, label: str) -> pd.DataFrame:
    """Gets an entire dataset and some metadata from an external source and returns it as
    a dataframe

    Parameters:
    -----------
    url : str
        A url where the data lives in json format
    subdomain : str
        A mediawiki project in the form of <language>.<project>, e.g. en.wikipedia
    label : str
        The label (either "goodfaith" or "damaging") to pull data for

    Returns:
    --------
    pd.Dataframe
        A dataframe containing the entire dataset
    """
    revisions = get_labels(url=url)
    revisions = get_all_features(
        revisions=revisions,
        subdomain=subdomain,
        label=label
    )
    revisions = get_metadata(
        revisions=revisions,
        subdomain=subdomain
    )
    return pd.DataFrame.from_dict(revisions, orient='index')

def split_data_and_labels(df: pd.DataFrame, model: Model, label: str, labels_bool: bool=True) -> Tuple[pd.DataFrame, pd.DataFrame]:
    """Splits a pandas dataframe into the features necessary for making predictions and
    the labels that correspond to those features

    Parameters:
    -----------
    df : pd.DataFrame
        Dataframe from which the data will be pulled
    model : revscoring.scoring.models.model.Model
        The model that will be scoring/predicting the data. Used to fetch the proper features.
    label : str
        The name of the column containing the labels to return
    labels_bool : boolean (default True)
        A boolean indicated whether the labels are encoded as boolean values. If yes,
        map them to a binary value. If no, return as is.

    Returns:
    --------
    X : pd.DataFrame
        DataFrame of relevant features
    y : pd.DataFrame
        List of labels
    """
    df.columns = df.columns.str.replace("feature.", "", regex=True)
    df = df.dropna()
    features = [f.name for f in model.features]
    for i, feature in enumerate(features):
        if feature == 'log((len(<datasource.tokenized(datasource.revision.parent.text)>) + 1))':
            features[i] = "log((len(<datasource.tokenized(<datasource.revision.parent.text>, 'Latin')>) + 1))"

    X = df[features]
    if labels_bool:
        y = df[label].map(lambda x: 1 if x else 0)
    else:
        y = df[label]
    return X, y

# This function is deprecated, but I'm keeping it in here because it's useful as
# a reminder of how to extract cache-encoded revscoring extract outputs
def get_decoded_labeled_revisions(path: str, label: str) -> Dict[int, Dict[str, Any]]:
    """Takes in an base-85 encoded json file of revisions (returned as an output of
    `revscoring extract...` shell command) and returns plaintext data dictionary.

    Parameters:
    -----------
    path : str
        Path to the encoded json file
    labels : str
        Label to extract, e.g. 'damaging', 'goodfaith'
    
    Returns:
    --------
    dictionary : {rev_id : {attribute : value}}
        Dictionary that maps rev_ids to rows represented by attribute value pairs
    """
    revisions = {}

    with open(path) as f:
        for line in f:
            observation = json.loads(line)
            if 'cache' in observation:
                observation['cache'] = pickle.loads(
                    base64.b85decode(bytes(observation['cache'], 'ascii')))
            else:
                continue

            revisions[observation['rev_id']] = {label: observation[label]}
            revisions[observation['rev_id']].update(observation['cache'])
            
    return revisions