from typing import Dict, Any, List
import utils
import pywikibot
import extract_data
import pandas as pd
from abstract_sheet import AbstractSheet
from sklearn import metrics
import os
import requests
from string import Template
import datetime

class ModelCard(AbstractSheet):
    """An object representing a Model Card. A ModelCard object inherits methods to
    implement from AbstractSheet, and implements other class methods as necessary.

    A ModelCard object also inherits parameters from AbstractSheet and adds on all
    necessary features to adapt it for model cards.
    """
    def __init__(
        # non-nullable features from AbstractSheet
        self,
        title: str,
        project: utils.Project,
        creator: utils.Creator,
        owner: utils.Owner,
        motivation: str,
        intended_users: str,
        intended_uses: str,
        banned_uses: str,
        most_recent_deploy: datetime.date,
        deprecation_conditions: List[str],
        approval_process: utils.ApprovalProcess,
        license: str,
    
        # non-nullable features from ModelCard
        dataset: str,
        mitigations: str,
        retraining: str,
        tier: int, 

        # nullable features from AbstractSheet
        sensitive_features: List[Dict[str, Any]]=None,

        # nullable features from ModelCard
        label: str=None,
        metrics: List[str]=None,
        model_url: str=None,
        model_info_url: str=None,
        test_data_url: str=None,
        train_data_url: str=None,
        api: str=None
    ) -> None:
        """Initializes a ModelCard object based on model card tier."""

        # non-nullable features from AbstractSheet (included in all tiers)
        param_dict = dict(
            title=title,
            project=project,
            creator=creator,
            owner=owner,
            motivation=motivation,
            intended_users=intended_users,
            intended_uses=intended_uses,
            banned_uses=banned_uses,
            most_recent_deploy=most_recent_deploy,
            approval_process=approval_process,
            deprecation_conditions=deprecation_conditions,
            license=license
        )

        # non-nullable features from ModelCard (included in all tiers)
        self.dataset = dataset
        self.mitigations = mitigations
        self.retraining = retraining
        self.tier = tier

        # if tier <= 2, fill in model_info_url and train_data_url
        if tier <= 2:
            self.model_info_url = model_info_url
            self.train_data_url = train_data_url

        # if tier <= 1, we can fill in everything else
        if tier <= 1:
            param_dict['sensitive_features'] = sensitive_features

            self.label = label
            self.metrics = metrics
            self.test_data_url = test_data_url
            self.model_url = model_url
            self.api = api
        
        super().__init__(**param_dict)
        

    def load_self(self) -> None:
        """Loads pickle of model from url in params object and unpacks features.
        Also loads training info from another url in params object and splits into
        model info, score schema, and training stats.

        Parameters:
        -----------
        self

        Returns:
        --------
        None
        """
        self.model = extract_data.get_model(self.model_url)
        self.features = self.model.features

    def load_model_info(self) -> None:
        """Loads model info and training stats from url in params object. Splits
        into model info, score schema, and training stats.

        Parameters:
        -----------
        self

        Returns:
        --------
        None
        """
        self.model_info, self.score_schema, self.train_stats = extract_data.get_model_info(self.model_info_url)


    def load_dataset(self) -> None:
        """Loads test data for a model, either from path stored in params object or
        from wikilabels semi-labeled dataset.

        Parameters:
        -----------
        self

        Returns:
        --------
        None
        """
        df = extract_data.get_data(
            url=self.test_data_url,
            subdomain=self.project.subdomain,
            label=self.label
        )
        
        # note: we store sensitive features separately from other data because if we
        # score with locally loaded model, we should only use the features for that model
        self.sensitive_data = extract_data.get_sensitive_features(df, self.sensitive_features)
        self.test_data, self.labels = extract_data.split_data_and_labels(
            df=df,
            model=self.model,
            label=self.label
        )

    def do_one_analysis(self, df: pd.DataFrame) -> Dict[str, Any]:
        """Conduct a statistical analysis on one subset of the broader test dataset.
        We do this analysis by filtering the test set prior to feeding it into this
        function. Once in this function, we iterate through possible metrics and
        calculate their values.

        Parameters:
        -----------
        df : pd.DataFrame
            pre-filtered dataframe to conduct analysis on
        
        Returns:
        --------
        {metric name : value}
            A dictionary of calculated metrics for this subset of the data to public
        """
        # TODO: add other modalities of scoring support (multiclass, NLP, etc.)
        # get labels, predictions, and probabilities
        y_true = df[self.label]
        y_pred = df['preds']
        y_prob = df['probs']

        analysis = {}

        # for each metric, see if it's in our recognized list; if yes, add to dictionary
        for metric in self.metrics:
            if metric == "roc_auc_score":
                try:
                    analysis['AUC Score'] = metrics.roc_auc_score(y_true, y_prob)
                except:
                    continue

            elif metric == "confusion_matrix":
                try:
                    (tn, fp, fn, tp) = metrics.confusion_matrix(y_true, y_pred).ravel()
                    analysis['True Positives'] = tp
                    analysis['True Negatives'] = tn
                    analysis['False Positives'] = fp
                    analysis['False Negatives'] = fn
                    analysis['True Positive Rate (Sensitivity)'] = tp / (tp + fn)
                    analysis['True Negative Rate (Specificity)'] = tn / (tn + fp)
                    analysis['False Positive Rate'] = fp / (fp + tn)
                    analysis['False Negative Rate'] = fn / (fn + tp)
                    analysis['Positive Predictive Value'] = tp / (tp + fp)
                    analysis['Negative Predictive Value'] = tn / (tn + fn)
                except:
                    continue

            elif metric == "classification_report":
                try:
                    grammar_map = {"0": "Negative sample", "1": "Positive sample"}
                    cr = metrics.classification_report(y_true, y_pred, output_dict=True)
                    analysis['Overall accuracy'] = cr['accuracy']
                    for label in ["0", "1"]:
                        for k, v in cr[label].items():
                            analysis[f"{grammar_map[label]} {k}"] = v
                except:
                    continue
            # TODO: add other metrics
            
        return analysis
  
    def do_analysis(self) -> Dict[str, Dict[str, Any]]:

        """Function that does all analyses for a particular model/test dataset.

        Parameters:
        -----------
        self

        Returns:
        --------
        analyses: {subset name : {metric name : value}}
            A dictionary of dictionaries representing individual analyses of the data
            on specific subsets of sensitive features.
        """

        analyses = {}
        
        # merge sensitive data and labels and other features so that we can filter by sensitive features
        df = self.test_data.merge(self.sensitive_data, left_index=True, right_index=True).merge(self.labels, left_index=True, right_index=True)

        # conduct statistical analysis on all data
        analyses['All data'] = self.do_one_analysis(df)

        # for each feature
        for feature in self.sensitive_features:
            # filter to specific condition
            if feature['col'] not in df.columns:
                col = feature['col'] + '_x'
            else:
                col = feature['col']

            have_feature, do_not_have_feature = utils.filter_df(
                df=df,
                col=col,
                cond=feature['cond'],
                val=feature['val']
            )

            # get feature name
            feature_name = feature['name']
            neg_feature_name = feature['neg_name']
            if feature_name is None:
                feature_name = feature['col'] + ' ' + feature['cond'] + ' ' + feature['val']
            if neg_feature_name is None:
                feature_name = 'not ' + feature['col'] + ' ' + feature['cond'] + ' ' + feature['val']
            
            # add analysis to that subset of data and its converse
            analyses[feature_name] = self.do_one_analysis(have_feature)
            analyses[neg_feature_name] = self.do_one_analysis(do_not_have_feature)
        
        return analyses
            

    def write_to_wiki(self, text: str) -> None:
        """Function for writing a completed analysis to a wiki in an automated
        fashion.

        Parameters:
        -----------
        self

        Returns:
        --------
        None
        """
        site = pywikibot.Site('meta', 'meta')
        page = pywikibot.Page(site, f"User:AlgoAccountabilityBot/{self.title} Model Card")
        page.text = text
        page.save(f"Updated {self.title} model card")

    def create_model_card_wikitext(self, analyses: Dict[str, Dict[str, Any]]=None) -> str:
        """Converts a  model card that has completed its analysis into a wikitext string
        that can then be uploaded to a wiki. Handles all three tiers of model cards.

        Parameters:
        -----------
        self

        analyses : {str : {str : val}} (optional)
            Dictionary of dictionaries; the output of do_analysis() that actually contains
            stats about the performance of the model on a test set

        Returns:
        --------
        str
            A long wikitext string that is comprised of all values from this analysis
            substituted into a template contained in the templates/ directory.
        """
        with open(f"templates/model_card_template_tier{self.tier}.txt", "r") as f:
            template = Template(f.read())

        # baseline (tier 3) template params
        param_dict = dict(
            title=self.title,
            tier=utils.tier_to_template(self.tier),
            motivation=self.motivation,
            creators=utils.list_creators_to_wikitext(self.creator),
            owners=str(self.owner),
            intended_users=self.intended_users,
            intended_uses=self.intended_uses,
            banned_uses=self.banned_uses,
            approval_process=str(self.approval_process),
            deprecation_conditions=utils.list_to_wikitext_bullets(self.deprecation_conditions),
            license=self.license,
            retraining=self.retraining,
            mitigations=self.mitigations,
            dataset=self.dataset
        )

        # if tier <= 2, add train_data_url, architecture, score schema, and train performance
        if self.tier <= 2:
            param_dict['train_data_url'] = self.train_data_url
            param_dict['architecture'] = utils.code_block_to_wikitext(self.model_info, lang="json")
            param_dict['score_schema'] = utils.code_block_to_wikitext(self.score_schema, lang="json")
            param_dict['train_performance'] = utils.code_block_to_wikitext(self.train_stats, lang="text")

        # if tier <= 1, add test_data_url test performance
        if self.tier <= 1:
            param_dict['test_data_url'] = self.test_data_url
            param_dict['test_performance'] = utils.df_to_wikitext_table(pd.DataFrame.from_dict(analyses, orient='index'))
            # param_dict['intersectional_performance'] =, # TODO: intersections of characteristics
            # param_dict['decision_threshold'] =, # TODO: decision threshold

        return template.safe_substitute(param_dict)

    def score_model(self) -> None:
        """Function that scores a model that is hosted through some API gateway
        in the cloud.

        Parameters:
        -----------
        self

        Returns:
        --------
        None
        """
        scores = {}

        # for the id of each entry
        for id in self.test_data.index.values:
            scores[id] = {}

            # try getting that id from the hosted model
            try:
                url = os.path.join(self.api, self.project.project, str(id), self.label)
                resp = requests.get(url)
                scores[id]['preds'] = resp.json()[self.project.project]['scores'][str(id)][self.label]['score']['prediction']
                scores[id]['probs'] = resp.json()[self.project.project]['scores'][str(id)][self.label]['score']['probability']['true']
            
            # if exception (bc of timeout, etc.) print response and continue
            except:
                print(resp.json())
                continue

        # merge with the original data using the ids as a join key to get rid of missed ids
        self.test_data = self.test_data.merge(pd.DataFrame.from_dict(scores, orient='index'), left_index=True, right_index=True)

    def test_deprecated(self) -> None:
        """Function that tests to see if this model is deprecated according to the
        deprecation conditions passed into it.

        Parameters:
        -----------
        self

        Returns:
        --------
        None
        """
        pass
