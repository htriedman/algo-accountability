from abc import ABC, abstractmethod
from datetime import datetime
from utils import Project, Creator, Owner, ApprovalProcess #, DeprecationConditions
from typing import Any, Dict, List

class AbstractSheet(ABC):
    """An abstract base class that contains all parameter objects and sets out all
    of the methods that an algorithmic accountability sheet should implement.

    Contains all features that are shared between model cards and datasheets.
    """
    def __init__(
        self, title: str, project: Project, creator: List[Creator], owner: Owner, motivation: str,
        intended_users: str, intended_uses: str, banned_uses: str, most_recent_deploy: datetime.date,
        deprecation_conditions: List[str], approval_process: ApprovalProcess, license: str,
        
        # nullable features
        sensitive_features: List[Dict[str, Any]]=None
    ) -> None:
        self.title = title
        self.project = project
        self.creator = creator
        self.owner = owner
        self.motivation = motivation
        self.intended_users = intended_users
        self.intended_uses = intended_uses
        self.banned_uses = banned_uses
        self.sensitive_features = sensitive_features
        self.most_recent_deploy = most_recent_deploy
        self.approval_process = approval_process
        self.deprecation_conditions = deprecation_conditions
        self.license = license

    @abstractmethod
    def load_self(self):
        pass

    @abstractmethod
    def do_analysis(self):
        pass

    @abstractmethod
    def write_to_wiki(self):
        pass

    @abstractmethod
    def test_deprecated(self) -> None:
        pass