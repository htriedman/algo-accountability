type: model
tier: 2
title: Enwiki WP 1.0
project: 
  language_code: en
  project: enwiki
  subdomain: en.wikipedia
creators:
  - name: Aaron Halfaker
    email: aaron.halfaker@gmail.com
    link: https://en.wikipedia.org/wiki/Aaron_Halfaker
  - name: Amir Sarabadani
    email: amir.sarabadani@wikimedia.de
    link: https://foundation.wikimedia.org/wiki/User:Ladsgroup
owner:
  name: WMF Machine Learning Team
  email: ml@wikimediafoundation.org
  link: https://www.mediawiki.org/wiki/Talk:Machine_Learning
motivation: >
  Article quality in Wikipedia is of critical concern. Many wikis implement a
  featured article process (e.g. en:Wikipedia:Featured article) for identifying
  high quality content and many WikiProjects use quality assessments to prioritize
  and direct work. But this assessment work is extremely time intensive and assessments
  will become out of date. This model makes predictions about article quality to support
  these processes.
intended_users: >
  English Wikipedia uses this model as a service for facilitating efficient reviews.
  of article quality. On an individual basis, anyone can submit a properly-formatted
  API call to ORES for a given article and get back the result of this model.
intended_uses: >
  This model should be used for facilitating article quality reviews on English
  Wikipedia.
banned_uses: >
  This model should not be used as an ultimate arbiter of whether or not an article
  is or is not high or low quality — a human should make that decision. It should not
  be used for any other English-language wiki besides English Wikipedia, and shouldn't
  be used for other languages.
most_recent_deploy: null
approval_process:
  description: >
    English Wikipedia decided (note: don't know where/when this decision was made,
    would love to find a link to that discussion) to use this model. Over time, the
    model has been validated through use in the community. The link below is just
    an example to show what this product might look like.
  forums:
    2021-09-07: https://example.com/
deprecation_conditions:
  - Data drift means training data for the model is no longer usable.
  - Doesn't meet desired performance metrics in production.
  - English Wikipedia community decides to not use this model anymore.
license: Creative Commons Attribution ShareAlike 3.0
retraining: >
  To my knowledge, this model has not been retrained over time — it still uses the
  original dataset from June of 2015.
mitigations: >
  This model does not mitigate data drift.
train_data_url: https://gitlab.wikimedia.org/htriedman/ores-data/-/raw/main/train/enwiki/enwiki.articlequality.nettrom_30k.json
model_info_url: https://gitlab.wikimedia.org/htriedman/ores-data/-/raw/main/model_info/enwiki/enwiki.nettrom_wp10.md
dataset: >
  This model was trained using hand-labeled training data from 2015. More details
  are available in the makefile of the articlequality github repository.