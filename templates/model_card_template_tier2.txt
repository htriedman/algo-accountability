= $title Model Card =

{{Algo-accountability}}

$tier

== Qualitative Analysis ==

=== What is the motivation behind creating this model? ===

$motivation
=== Who created this model? ===

$creators

=== Who currently owns/is responsible for this model? ===

$owners

=== Who are the intended users of this model? ===

$intended_users
=== What should this model be used for? ===

$intended_uses
=== What should this model not be used for? ===

$banned_uses
=== What community approval processes has this model gone through? ===

$approval_process

=== What internal or external changes could make this model deprecated or no longer usable? ===

$deprecation_conditions

=== How should this model be licensed? ===

$license

=== If this model is retrained, can we see how it has changed over time? ===

$retraining
=== How does this model mitigate data drift? ===

$mitigations
=== Which service(s) rely on this model? ===

This model is one of many models that powers ORES, the Wikimedia Foundation's machine
machine learning API.

Learn more about ORES [http://ores.wikimedia.org/ here]

=== Which dataset(s) does this model rely on? ===

$dataset

Train dataset is available for download [$train_data_url here]

== Quantitative Analysis ==

=== How did the model perform on training data? ===

$train_performance

== Model Information ==
=== What is the architecture of this model? ===

$architecture

=== What is the score schema this model returns? ===

$score_schema
