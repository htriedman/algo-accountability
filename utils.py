from binascii import hexlify
from ipaddress import ip_address, IPv4Address
import socket
import struct
from typing import Any, Dict, List, Tuple
import datetime
import pandas as pd
import re

class Creator:
    def __init__(self, name: str, email: str, link: str=None) -> None:
        self.name = name
        self.email = email
        self.link = link

    def __str__(self) -> str:
        if self.link is not None:
            return f"[{self.link} {self.name}] ({self.email})"
        else:
            return f"{self.name} ({self.email})"

class Owner:
    def __init__(self, name: str, email: str, link: str=None) -> None:
        self.name = name
        self.email = email
        self.link = link

    def __str__(self) -> str:
        if self.link is not None:
            return f"[{self.link} {self.name}] ({self.email})"
        else:
            return f"{self.name} ({self.email})"

class Project:
    def __init__(self, language_code: str, project: str, subdomain: str) -> None:
        self.language_code = language_code
        self.project = project
        self.subdomain = subdomain

class ApprovalProcess:
    def __init__(self, description: str, forums: Dict[datetime.date, str]) -> None:
        self.description = description
        self.forums = forums

    def __str__(self) -> str:
        output = f"{self.description}\n"
        output += "==== Dates of consideration forums ====\n"
        output += list_to_wikitext_bullets([f"[{url} {dt}]" for dt, url in self.forums.items()])
        return output
        
# TODO: add callable deprecation functions to this logic for automated deprecation
# class DeprecationConditions:
#     def __init__(self, conditions: List[Callable]) -> None:
#         self.conditions = conditions

def validIPAddress(ip: str) -> str:
    """A function that takes in a IP address string and returns a string that
    represents either an IPv4 or IPv6 address

    Parameters:
    -----------
    ip : str
        An IP address represented as a string

    Returns:
    --------
    str
        A string — either "IPv4", "IPv6", or "Invalid — representing IP type/validity

    Raises:
    -------
    ValueError
        If IP address is not a valid IP
    """
    try:
        return "IPv4" if type(ip_address(ip)) is IPv4Address else "IPv6"
    except ValueError:
        return "Invalid"

def ip2long(ip: str, kind: str) -> int(16):
    """Convert an IP string to long integer. Called once it is clear that an IP
    address is determined to be valid.

    Parameters:
    -----------
    ip : str
        An IP address represented as a string
    kind : str (either "IPv4" or "IPv6")
        A string saying which kind of IP address it is

    Returns:
    --------
    int(16)
        A long int representing the value of the IP address
    """
    if kind == "IPv4":
        packedIP = socket.inet_aton(ip)
        return struct.unpack("!L", packedIP)[0]
    else:
        return int(hexlify(socket.inet_pton(socket.AF_INET6, ip)), 16)

def filter_df(df: pd.DataFrame, col: str, cond: str, val: Any, dropna: bool=False) -> Tuple[pd.DataFrame, pd.DataFrame]:
    """A function that generically filters a dataframe based on an input condition
    and some value. For example, given a dataframe df and:

    col = 'test_col'
    cond = 'eq'
    val = True

    this function returns (df[df['test_col'] == True], df[df['test_col'] != True])

    Parameters:
    -----------
    df : pd.DataFrame
        Dataframe to be filtered
    col : str
        The name of the column to filter on
    cond : str (one of 'eq', 'neq', 'gt', 'ge', 'lt', 'le', 'in', 'not in', 'is', 'is not')
        The condition to filter with
    val : Any
        The value to filter based upon

    Returns:
    --------
    (pd.DataFrame, pd.DataFrame)
        Two dataframes, one satisfying the condition and the other satisfying the 
        opposite condition
    """
    if cond == "eq":
        included = df[df[col] == val]
        excluded = df[df[col] != val]
    elif cond == "neq":
        included = df[df[col] != val]
        excluded = df[df[col] == val]
    elif cond == "gt":
        included = df[df[col] > val]
        excluded = df[df[col] <= val]
    elif cond == "ge":
        included = df[df[col] >= val]
        excluded = df[df[col] < val]
    elif cond == "lt":
        included = df[df[col] < val]
        excluded = df[df[col] >= val]
    elif cond == "le":
        included = df[df[col] <= val]
        excluded = df[df[col] > val]
    elif cond == "in":
        included = df[df[col].apply(lambda x: val in x)]
        excluded = df[df[col].apply(lambda x: val not in x)]
    elif cond == "not in":
        included = df[df[col].apply(lambda x: val not in x)]
        excluded = df[df[col].apply(lambda x: val in x)]
    elif cond == "is":
        included = df[df[col] is val]
        excluded = df[df[col] is not val]
    elif cond == "is not":
        included = df[df[col] is not val]
        excluded = df[df[col] is val]

    if dropna:
        included = included.dropna()
        excluded = excluded.dropna()
    
    return included, excluded

# All of the code below is grammar for converting various code objects to wikitext
# When these library is separated out into a statistical lib and a template/text lib,
# all of these functions will be in the template/text lib.

def tier_to_template(tier: int) -> str:
    return f"{{{{Model-card-tier{tier}}}}}"

def code_block_to_wikitext(code: str, lang: str) -> str:
    return f"<syntaxhighlight lang='{lang}' line >\n{code}\n</syntaxhighlight>"

def list_creators_to_wikitext(creators: List[Creator]) -> str:
    output = ""
    if len(creators) == 0:
        output = "Creators not listed — check Gitlab for more info"
    elif len(creators) == 1:
        output = str(creators[0])
    elif len(creators) == 2:
        output = " and ".join([str(creator) for creator in creators])
    else:
        for i, creator in enumerate(creators):
            if i == len(creators)-1:
                output += "and " + str(creator)
            else:
                output += str(creator) + ", "
    
    output += "."
    return output

def model_architecture_to_wikitext(result: Dict[str, Any]) -> str:
    output = f"Model Type: {result['Model Type']}\n"
    output += "==== Model Parameters ====\n"
    output += list_to_wikitext_bullets([f"{param}: {value}" for param, value in result['Model Params'].items()])
    return output

def list_to_wikitext_bullets(items: List[str]) -> str:
    output = ""
    for item in items:
        output += f"* {item}\n"
    return output[:-1] # cut off final newline

def df_to_wikitext_table(df: pd.DataFrame) -> str:
    output = '{| class="wikitable"\n!\n'
    for col in df.columns:
        output += f"!{col}\n"

    output += "|-\n"
    for tuple in df.itertuples():
        output += f"!{tuple.Index}\n"
        for val in tuple[1:]:
            if isinstance(val, float):
                output += f"|{truncate(val)}\n"
            else:
                output += f"|{val}\n"
        output += "|-\n"

    output += "|}"
    return output

def truncate(num):
    return re.sub(r'^(\d+\.\d{,3})\d*$',r'\1',str(num))